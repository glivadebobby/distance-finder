package in.glivade.distancefinder;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity
        implements SurfaceHolder.Callback, TextToSpeech.OnInitListener {

    private static final int FACE_SCAN_INTERVAL = 5000;
    private TextView textViewDistance;
    private Camera camera;
    private SurfaceHolder surfaceHolder;
    private boolean previewing = false;
    private long lastScannedTime;
    private DrawingView drawingView;
    private Face[] detectedFaces;
    private TextToSpeech tts;
    private AutoFocusCallback myAutoFocusCallback = new AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean arg0, Camera arg1) {
            if (arg0) {
                camera.cancelAutoFocus();
            }
            float focusDistances[] = new float[3];
            arg1.getParameters().getFocusDistances(focusDistances);
            float cameraDistance = focusDistances[Camera.Parameters.FOCUS_DISTANCE_OPTIMAL_INDEX];
            if (cameraDistance == Float.POSITIVE_INFINITY) return;
            NumberFormat numberformat = NumberFormat.getInstance();
            numberformat.setMaximumFractionDigits(2);
            String obstacle = "Obstacle in " + numberformat.format(cameraDistance) + " meters";
            tts.speak(obstacle, TextToSpeech.QUEUE_FLUSH, null);
            textViewDistance.setText(obstacle);
        }
    };
    private FaceDetectionListener faceDetectionListener = new FaceDetectionListener() {
        @Override
        public void onFaceDetection(Face[] faces, Camera camera1) {
            if (faces.length == 0) {
                drawingView.setHaveFace(false);
            } else {
                if (lastScannedTime + FACE_SCAN_INTERVAL > System.currentTimeMillis()) return;
                lastScannedTime = System.currentTimeMillis();
                drawingView.setHaveFace(true);
                detectedFaces = faces;
                List<Camera.Area> focusList = new ArrayList<>();
                Camera.Area firstFace = new Camera.Area(faces[0].rect, 1000);
                focusList.add(firstFace);
                if (camera.getParameters().getMaxNumFocusAreas() > 0) {
                    camera.getParameters().setFocusAreas(focusList);
                }
                if (camera.getParameters().getMaxNumMeteringAreas() > 0) {
                    camera.getParameters().setMeteringAreas(focusList);
                }
                ScheduledExecutorService myScheduledExecutorService = Executors.newScheduledThreadPool(1);
                myScheduledExecutorService.schedule(new Runnable() {
                    public void run() {
                        camera.autoFocus(myAutoFocusCallback);
                    }
                }, 200, TimeUnit.MILLISECONDS);
            }
            drawingView.invalidate();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewDistance = findViewById(R.id.txt_distance);
        SurfaceView surfaceView = findViewById(R.id.camera_preview);
        tts = new TextToSpeech(this, this);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        drawingView = new DrawingView(this);
        LayoutParams layoutParamsDrawing = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addContentView(drawingView, layoutParamsDrawing);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (previewing) {
            camera.stopFaceDetection();
            camera.stopPreview();
            previewing = false;
        }
        if (camera != null) {
            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                camera.startFaceDetection();
                previewing = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        camera = Camera.open();
        camera.setFaceDetectionListener(faceDetectionListener);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopFaceDetection();
        camera.stopPreview();
        camera.release();
        camera = null;
        previewing = false;
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.getDefault());
        }
    }

    private class DrawingView extends View {

        private Paint drawingPaint;
        private boolean haveFace;

        public DrawingView(Context context) {
            super(context);
            haveFace = false;
            drawingPaint = new Paint();
            drawingPaint.setColor(Color.GREEN);
            drawingPaint.setStyle(Paint.Style.STROKE);
            drawingPaint.setStrokeWidth(2);
        }

        public void setHaveFace(boolean h) {
            haveFace = h;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            if (haveFace) {
                int vWidth = getWidth();
                int vHeight = getHeight();
                for (int i = 0; i < detectedFaces.length; i++) {
                    if (i == 0) {
                        drawingPaint.setColor(Color.GREEN);
                    } else {
                        drawingPaint.setColor(Color.RED);
                    }
                    int l = detectedFaces[i].rect.left;
                    int t = detectedFaces[i].rect.top;
                    int r = detectedFaces[i].rect.right;
                    int b = detectedFaces[i].rect.bottom;
                    int left = (l + 1000) * vWidth / 2000;
                    int top = (t + 1000) * vHeight / 2000;
                    int right = (r + 1000) * vWidth / 2000;
                    int bottom = (b + 1000) * vHeight / 2000;
                    canvas.drawRect(left, top, right, bottom, drawingPaint);
                }
            } else {
                canvas.drawColor(Color.TRANSPARENT);
            }
        }
    }
}
